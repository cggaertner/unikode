my @ranges = sort *.key.bounds[0], 'GraphemeBreakProperty.txt'.IO.lines.map: {
    (:16(~$0) .. :16(~($1 // $0))) => ~$2
        if /^ (<xdigit>+) [ '..' (<xdigit>+) ]? \s+ ';' \s+ (\w+) /;
}

my $prev := @ranges[0];
for @ranges[1..*] -> $curr is rw {
    my ($pa, $pb) = $prev.key.bounds;
    my ($ca, $cb) = $curr.key.bounds;

    if $pb + 1 == $ca && $prev.value eq $curr.value {
        $prev = ($pa .. $cb) => $curr.value;
        $curr = Any;
    }
    else { $prev := $curr }
}

my $code = join "\n    ", gather {
    my $pb = 0;
    for @ranges.grep(*.defined) {
        my ($a, $b) = .key.bounds;

        take sprintf 'if(cp  < 0x%04X) return UNIGCB_Other;', $a
            if $pb + 1 < $a;

        take sprintf 'if(cp %s= 0x%04X) return UNIGCB_%s;',
            $a < $b ?? '<' !! '=', $b, .value;

        $pb = $b;
    }
}

printf q:to/__END__/, $code;
#ifndef UNIGCB_H_
#define UNIGCB_H_

#include <stdint.h>

enum {
    UNIGCB_FLAG_CONTROL = 1 << 4,
    UNIGCB_FLAG_POSTFIX = 1 << 5,
    UNIGCB_FLAG_PREFIX  = 1 << 6,
    UNIGCB_FLAG_REGIO   = 1 << 7,

    UNIGCB_FLAG_L       = 1 << 8,
    UNIGCB_FLAG_V       = 1 << 9,
    UNIGCB_FLAG_T       = 1 << 10,
    UNIGCB_MASK_HANGUL  = UNIGCB_FLAG_L | UNIGCB_FLAG_V | UNIGCB_FLAG_T,

    UNIGCB_Other              = 0,
    UNIGCB_CR                 = UNIGCB_FLAG_CONTROL | 1,
    UNIGCB_LF                 = UNIGCB_FLAG_CONTROL | 2,
    UNIGCB_Control            = UNIGCB_FLAG_CONTROL,
    UNIGCB_Prepend            = UNIGCB_FLAG_PREFIX  | 1,
    UNIGCB_Extend             = UNIGCB_FLAG_POSTFIX | 1,
    UNIGCB_SpacingMark        = UNIGCB_FLAG_POSTFIX | 2,
    UNIGCB_Regional_Indicator = UNIGCB_FLAG_REGIO,

    UNIGCB_L   = UNIGCB_FLAG_L,
    UNIGCB_V   = UNIGCB_FLAG_V,
    UNIGCB_T   = UNIGCB_FLAG_T,
    UNIGCB_LV  = UNIGCB_FLAG_L | UNIGCB_FLAG_V,
    UNIGCB_LVT = UNIGCB_FLAG_L | UNIGCB_FLAG_V | UNIGCB_FLAG_T,
};

typedef uint_fast32_t unigcb_t;

#define GCB_MASK(L, R) ((unigcb_t)((L) << 16 | (R)))

inline _Bool unigcb_isbreak(unigcb_t left, unigcb_t right) {
    const unigcb_t CONTROL = GCB_MASK(
        UNIGCB_FLAG_CONTROL,
        UNIGCB_FLAG_CONTROL);

    const unigcb_t CR_LF = GCB_MASK(
        UNIGCB_CR,
        UNIGCB_LF);

    const unigcb_t PRE_POST = GCB_MASK(
        UNIGCB_FLAG_PREFIX,
        UNIGCB_FLAG_POSTFIX);

    const unigcb_t REGIO_REGIO = GCB_MASK(
        UNIGCB_FLAG_REGIO,
        UNIGCB_FLAG_REGIO);

    const unigcb_t V_V = GCB_MASK(
        UNIGCB_FLAG_V,
        UNIGCB_FLAG_V);

    const unigcb_t L_ZERO = GCB_MASK(
        UNIGCB_FLAG_L,
        0);

    unigcb_t mask = GCB_MASK(left, right);

    if(mask & CONTROL)
        return mask ^ CR_LF;

    if(mask & PRE_POST)
        return 0;

    if(mask == REGIO_REGIO)
        return 0;

    if(!(left & UNIGCB_MASK_HANGUL) || !(right & UNIGCB_MASK_HANGUL))
        return 1;

    if(left == UNIGCB_L && right != UNIGCB_T)
        return 0;

    if(left != UNIGCB_L && right == UNIGCB_T)
        return 0;

    if((mask ^ V_V) == 0 || (mask ^ V_V) == L_ZERO)
        return 0;

    return 1;
}

#undef GCB_MASK

inline unigcb_t unigcb_classof(uint32_t cp) {
    %s
    return UNIGCB_Other;
}

static inline _Bool unigcb(uint32_t cpl, uint32_t cpr) {
    return unigcb_isbreak(unigcb_classof(cpl), unigcb_classof(cpr));
}

#endif
__END__
