#include "unigcb.h"

extern unigcb_t unigcb_classof(uint32_t);
extern _Bool unigcb_isbreak(unigcb_t, unigcb_t);
